from PyQt5.QtWidgets import QDialog
from PyQt5.Qt import pyqtSignal
from Ui_SimButtonControl import Ui_SimButtonControl


class SimButtonControl(QDialog):

    button_record_pushed = pyqtSignal()
    button_main_pushed = pyqtSignal()
    button_one_pushed = pyqtSignal()
    button_two_pushed = pyqtSignal()
    button_three_pushed = pyqtSignal()

    def __init__(self, parent):
        super(QDialog, self).__init__(parent)

        self.setModal(False)
        self.ui = Ui_SimButtonControl()
        self.ui.setupUi(self)

        self.setWindowTitle("Button Simulator")

        self.ui.btn_main.clicked.connect(self.main_clicked)
        self.ui.btn_record.clicked.connect(self.record_clicked)
        self.ui.btn_one.clicked.connect(self.one_clicked)
        self.ui.btn_two.clicked.connect(self.two_clicked)
        self.ui.btn_three.clicked.connect(self.three_clicked)

    def main_clicked(self):
        self.button_main_pushed.emit()

    def record_clicked(self):
        self.button_record_pushed.emit()

    def one_clicked(self):
        self.button_one_pushed.emit()

    def two_clicked(self):
        self.button_two_pushed.emit()

    def three_clicked(self):
        self.button_three_pushed.emit()
