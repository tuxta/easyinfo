import os
import os.path
import cv2
import time
import ffmpeg
from PyQt5 import QtMultimedia
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QLabel
from PyQt5.QtCore import Qt, QThread, pyqtSignal, pyqtSlot, QUrl


class RecordingView(QLabel):
    def __init__(self):
        super().__init__()

        self.video_done = False
        self.audio_done = False
        self.button_number = 0
        self.th1 = VideoThread()
        self.audio_thread = AudioRecorder()
        self.th1.change_pixmap.connect(self.set_image)
        self.th1.video_done.connect(self.video_finished_recording)
        self.audio_thread.audio_done.connect(self.audio_finished_recording)

    @pyqtSlot(QImage)
    def set_image(self, image):
        final_image = image.scaled(self.width(), self.height(), Qt.KeepAspectRatio)
        self.setPixmap(QPixmap.fromImage(final_image))

    def start(self, button):
        self.button_number = button

        self.th1.start()
        self.audio_thread.start()

    def stop(self):
        self.th1.stop()
        self.audio_thread.stop()

    def video_finished_recording(self):
        self.video_done = True
        if self.audio_done:
            self.process_video_audio()

    def audio_finished_recording(self):
        self.audio_done = True
        if self.video_done:
            self.process_video_audio()

    def process_video_audio(self):
        video = ffmpeg.input(f'vid.mp4')
        audio = ffmpeg.input(f"audio.mp3")
        filename = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            f"final_vid_{self.button_number}.mp4"
        )
        if os.path.exists(filename):
            os.remove(filename)
        out = ffmpeg.output(
            video,
            audio,
            filename,
            vcodec='copy',
            acodec='aac',
            strict='experimental',
            loglevel='error'
        )
        out.run()
        self.video_done = False
        self.audio_done = False


class VideoThread(QThread):
    change_pixmap = pyqtSignal(QImage, name='image')
    video_done = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.capture = None
        self.frames = []
        self.capturing = False
        self.time_start = None
        self.time_end = None

    def run(self):
        self.frames = []
        self.capturing = True
        self.capture = cv2.VideoCapture(0)
        self.time_start = time.time()
        while self.capturing:
            ret, frame = self.capture.read()
            if ret:
                frame = cv2.flip(frame, 1)
                self.frames.append(frame)
                rgb_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                h, w, ch = rgb_image.shape
                bytes_per_line = ch * w
                qt_format = QImage(
                    rgb_image.data, w, h, bytes_per_line, QImage.Format_RGB888)
                self.change_pixmap.emit(qt_format)

    def stop(self):
        self.capturing = False
        self.time_end = time.time()
        video_width = int(self.capture.get(cv2.CAP_PROP_FRAME_WIDTH))
        video_height = int(self.capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
        self.capture.release()
        self.capture = None

        fourcc = cv2.VideoWriter_fourcc(*'mp4v')
        total_time = self.time_end - self.time_start
        fps = len(self.frames) / total_time
        out_file = cv2.VideoWriter(f"vid.mp4", fourcc, fps, (video_width, video_height), True)
        padding = int(fps / 2)
        for i in range(padding):
            out_file.write(self.frames[0])
        for frame in self.frames:
            out_file.write(frame)
        out_file.release()

        self.video_done.emit()


class AudioRecorder(QThread):
    audio_done = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.recorder = QtMultimedia.QAudioRecorder()
        audio_input = self.recorder.audioInput()
        self.recorder.setAudioInput(audio_input)
        settings = QtMultimedia.QAudioEncoderSettings()
        settings.setCodec("")
        settings.setSampleRate(0)
        settings.setBitRate(0)
        settings.setChannelCount(-1)
        settings.setQuality(QtMultimedia.QMultimedia.NormalQuality)
        settings.setEncodingMode(QtMultimedia.QMultimedia.ConstantBitRateEncoding)
        self.recorder.setEncodingSettings(
            settings,
            QtMultimedia.QVideoEncoderSettings(), ""
        )
        file_name = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            f"audio.mp3"
        )
        self.recorder.setOutputLocation(QUrl.fromLocalFile(file_name))

    def run(self):
        self.recorder.record()

    def stop(self):
        self.recorder.pause()
        self.recorder.stop()
        self.audio_done.emit()
