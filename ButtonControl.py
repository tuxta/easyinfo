import RPi.GPIO as GPIO
from PyQt5.Qt import QObject, pyqtSignal, pyqtSlot


class ButtonControl(QObject):
    button_record_pushed = pyqtSignal()
    button_main_pushed = pyqtSignal()
    button_one_pushed = pyqtSignal()
    button_two_pushed = pyqtSignal()
    button_three_pushed = pyqtSignal()

    def __init__(self):
        super().__init__()
        # Set up GPIO Buttons
        GPIO.setmode(GPIO.BOARD) # Use pin numbering on the board
        GPIO.setup(10, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(12, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

        GPIO.add_event_detect(10, GPIO.RISING, callback=self.button_record_pressed)
        GPIO.add_event_detect(12, GPIO.RISING, callback=self.button_main_pressed)
        GPIO.add_event_detect(16, GPIO.RISING, callback=self.button_one_pressed)
        GPIO.add_event_detect(18, GPIO.RISING, callback=self.button_two_pressed)
        GPIO.add_event_detect(22, GPIO.RISING, callback=self.button_three_pressed)

    @pyqtSlot()
    def button_record_pressed(self, channel):
        self.button_record_pushed.emit()

    @pyqtSlot()
    def button_main_pressed(self, channel):
        self.button_main_pushed.emit()

    @pyqtSlot()
    def button_one_pressed(self, channel):
        self.button_one_pushed.emit()

    @pyqtSlot()
    def button_two_pressed(self, channel):
        self.button_two_pushed.emit()

    @pyqtSlot()
    def button_three_pressed(self, channel):
        self.button_three_pushed.emit()

    def show(self):
        pass

    def __del__(self):
        GPIO.cleanup()
