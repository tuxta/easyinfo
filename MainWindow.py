import os
from enum import IntEnum
from PyQt5.QtGui import QPixmap
from PyQt5.Qt import QUrl, QTimer
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtMultimedia import QMediaPlayer, QMediaContent
from PyQt5.QtWidgets import QMainWindow, QSizePolicy, QProgressBar

from Ui_MainWindow import Ui_MainWindow
from RecordingView import RecordingView


class States(IntEnum):
    HOME = 0
    PLAY_BACK = 1
    RECORD_SELECT = 2
    RECORDING = 3


class MainWindow(QMainWindow):

    def __init__(self, deployed):
        super().__init__()

        self.curr_state = States.HOME
        self.can_push_button = True

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowTitle('Easy Info')

        if deployed:
            from ButtonControl import ButtonControl
            self.button_control = ButtonControl()
        else:
            from SimButtonControl import SimButtonControl
            self.button_control = SimButtonControl(self)

        self.main_image = QPixmap('Button_Press.png')
        self.select_image = QPixmap('Record_Select.png')
        self.ui.image_label.setPixmap(self.main_image)
        self.ui.image_label.setScaledContents(True)

        self.ui.stackedWidget.setCurrentWidget(self.ui.image)

        self.video_widget = QVideoWidget(self.ui.stackedWidget)
        self.video_widget.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.recording_view = RecordingView()
        self.recording_view.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.progress_bar = QProgressBar(self.ui.stackedWidget)
        self.progress_bar.setValue(0)

        self.ui.video.layout().addWidget(self.video_widget)
        self.ui.video.layout().addWidget(self.progress_bar)

        self.ui.recording.layout().addWidget(self.recording_view)

        self.mediaPlayer = QMediaPlayer(None, QMediaPlayer.VideoSurface)
        self.mediaPlayer.setVideoOutput(self.video_widget)
        self.mediaPlayer.setNotifyInterval(200)

        self.mediaPlayer.positionChanged.connect(self.position_changed)
        self.mediaPlayer.stateChanged.connect(self.play_back_changed)
        
        self.button_control.button_record_pushed.connect(self.set_record)
        self.button_control.button_main_pushed.connect(lambda button=0: self.play(button))
        self.button_control.button_one_pushed.connect(lambda button=1: self.play(button))
        self.button_control.button_two_pushed.connect(lambda button=2: self.play(button))
        self.button_control.button_three_pushed.connect(lambda button=3: self.play(button))

        if deployed:
            self.showFullScreen()

    def keyPressEvent(self, e):
        if e.text() == 'w':
            self.showNormal()
        elif e.text() == 'f':
            self.showFullScreen()

    def show(self):
        super(MainWindow, self).show()
        self.button_control.show()

    def position_changed(self, position):
        self.progress_bar.setMaximum(self.mediaPlayer.duration() / 100)
        self.progress_bar.setValue(position / 100)
    
    def play_back_changed(self, state):
        if state == 0:
            self.ui.stackedWidget.setCurrentWidget(self.ui.image)
            self.curr_state = States.HOME

    def play(self, button):
        if self.can_push_button:
            if self.curr_state == States.RECORD_SELECT:
                self.curr_state = States.RECORDING
                self.can_push_button = False
                QTimer.singleShot(1000, self.reset_can_push_button)
                self.record(button)
            elif self.curr_state == States.HOME:
                self.curr_state = States.PLAY_BACK
                self.ui.stackedWidget.setCurrentWidget(self.ui.video)
                video_url = os.path.join(os.path.abspath(os.getcwd()), f"final_vid_{button}.mp4")
                self.mediaPlayer.setMedia(QMediaContent(QUrl.fromLocalFile(video_url)))
                self.mediaPlayer.play()
                self.can_push_button = False
                QTimer.singleShot(1000, self.reset_can_push_button)

    def set_record(self):
        if self.can_push_button:
            if self.curr_state == States.HOME:
                self.curr_state = States.RECORD_SELECT
                self.ui.image_label.setPixmap(self.select_image)
                self.ui.image_label.setScaledContents(True)
                self.can_push_button = False
                QTimer.singleShot(1000, self.reset_can_push_button)
            elif self.curr_state == States.RECORDING:
                self.stop_recording()
                self.ui.image_label.setPixmap(self.main_image)
                self.ui.image_label.setScaledContents(True)
                self.can_push_button = False
                QTimer.singleShot(1000, self.reset_can_push_button)

    def record(self, button):
        self.ui.stackedWidget.setCurrentWidget(self.ui.recording)
        self.recording_view.start(button)

    def stop_recording(self):
        self.recording_view.stop()
        self.ui.stackedWidget.setCurrentWidget(self.ui.image)
        self.curr_state = States.HOME

    def reset_can_push_button(self):
        self.can_push_button = True
