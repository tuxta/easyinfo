 
import sys
from PyQt5.QtWidgets import QApplication
from MainWindow import MainWindow


def main(deployed):
    app = QApplication(sys.argv)
    app.setOrganizationName("Tuxtas")
    main_win = MainWindow(deployed)
    main_win.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == "sim":
        main(False)
    else:
        main(True)
